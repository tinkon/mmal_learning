package com.mmall.controller.admin;

import com.google.common.collect.Maps;
import com.mmall.common.ResponseCode;
import com.mmall.common.ServerResponse;
import com.mmall.pojo.Product;
import com.mmall.pojo.User;
import com.mmall.service.IFileService;
import com.mmall.service.IProductService;
import com.mmall.service.IUserService;
import com.mmall.util.CookieUtil;
import com.mmall.util.JsonUtil;
import com.mmall.util.PropertiesUtil;
import com.mmall.util.RedisShardedPoolUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

@Controller
@RequestMapping("/manage/product/")
public class ProductManageController {

    @Autowired
    private IUserService iUserService;

    @Autowired
    private IProductService iProductService;

    @Autowired
    private IFileService iFileService;

    @RequestMapping(value = "save.do")
    @ResponseBody
    public ServerResponse productSave(Product product, HttpServletRequest httpServletRequest){
//        User user = (User) session.getAttribute(Const.CURRENT_USER);
        //二期代码
//        String loginToken = CookieUtil.readLoginToken(httpServletRequest);
//        if (StringUtils.isEmpty(loginToken)){
//            return ServerResponse.createByErrorMessage("用户未登录,无法获取当前用户信息");
//        }
//        String userJsonStr = RedisShardedPoolUtil.get(loginToken);
//        User user = JsonUtil.string2Obj(userJsonStr,User.class);
//        if(user ==null){
//            return ServerResponse.createByErrorCodeMessage(ResponseCode.NEED_LOGIN.getCode(),"用户未登录，请登录");
//        }
//        if(iUserService.checkAdminRole(user).isSuccess()){
//            //填充业务
//            return iProductService.manageSaveOrUpdateProduct(product);
//        }else{
//            return ServerResponse.createByErrorMessage("无权限操作，需要管理员权限");
//        }
        //全部通过拦截器验证是否登录以及权限
        return iProductService.manageSaveOrUpdateProduct(product);

    }

    @RequestMapping(value = "set_sale_status.do")
    @ResponseBody
    public ServerResponse setSaleStatus( Integer  productId,Integer status, HttpServletRequest httpServletRequest){
//        User user = (User) session.getAttribute(Const.CURRENT_USER);
        //二期代码
//        String loginToken = CookieUtil.readLoginToken(httpServletRequest);
//        if(StringUtils.isEmpty(loginToken)){
//            return ServerResponse.createByErrorMessage("用户未登录,无法获取当前用户信息");
//        }
//        String userJsonStr = RedisShardedPoolUtil.get(loginToken);
//        User user = JsonUtil.string2Obj(userJsonStr,User.class);
//        if(user ==null){
//            return ServerResponse.createByErrorCodeMessage(ResponseCode.NEED_LOGIN.getCode(),"用户未登录，请登录");
//        }
//        if(iUserService.checkAdminRole(user).isSuccess()){
//            //填充业务
//            return iProductService.manageSetSaleStatus(productId,status);
//        }else{
//            return ServerResponse.createByErrorMessage("无权限操作，需要管理员权限");
//        }

        return iProductService.manageSetSaleStatus(productId,status);

    }


    @RequestMapping(value = "detail.do")
    @ResponseBody
    public ServerResponse getProductDetail(Integer  productId, HttpServletRequest httpServletRequest){
//        User user = (User) session.getAttribute(Const.CURRENT_USER);

        //二期代码
//        String loginToken = CookieUtil.readLoginToken(httpServletRequest);
//        if(StringUtils.isEmpty(loginToken)){
//            return ServerResponse.createByErrorMessage("用户未登录,无法获取当前用户信息");
//        }
//        String userJsonStr = RedisShardedPoolUtil.get(loginToken);
//        User user = JsonUtil.string2Obj(userJsonStr,User.class);
//        if(user ==null){
//            return ServerResponse.createByErrorCodeMessage(ResponseCode.NEED_LOGIN.getCode(),"用户未登录，请登录");
//        }
//        if(iUserService.checkAdminRole(user).isSuccess()){
//            //填充业务
//            return iProductService.manageProductDetail(productId);
//        }else{
//            return ServerResponse.createByErrorMessage("无权限操作，需要管理员权限");
//        }

        return iProductService.manageProductDetail(productId);

    }

    @RequestMapping(value = "list.do")
    @ResponseBody
    public ServerResponse getList(HttpServletRequest httpServletRequest,
                                  @RequestParam(value = "pageNum",defaultValue = "1") Integer  pageNum,
                                  @RequestParam(value = "pageSize",defaultValue = "10") int pageSize){
//        User user = (User) session.getAttribute(Const.CURRENT_USER);

        //二期代码
//        String loginToken = CookieUtil.readLoginToken(httpServletRequest);
//        if(StringUtils.isEmpty(loginToken)){
//            return ServerResponse.createByErrorMessage("用户未登录,无法获取当前用户信息");
//        }
//        String userJsonStr = RedisShardedPoolUtil.get(loginToken);
//        User user = JsonUtil.string2Obj(userJsonStr,User.class);
//        if(user ==null){
//            return ServerResponse.createByErrorCodeMessage(ResponseCode.NEED_LOGIN.getCode(),"用户未登录，请登录");
//        }
//        if(iUserService.checkAdminRole(user).isSuccess()){
//            //填充业务
//            return iProductService.manageGetProductList(pageNum, pageSize);
//        }else{
//            return ServerResponse.createByErrorMessage("无权限操作，需要管理员权限");
//        }

        return iProductService.manageGetProductList(pageNum, pageSize);

    }


    @RequestMapping(value = "search.do")
    @ResponseBody
    public ServerResponse productSearch(HttpServletRequest httpServletRequest,
                                  String productName,Integer productId,
                                  @RequestParam(value = "pageNum",defaultValue = "1") Integer  pageNum,
                                  @RequestParam(value = "pageSize",defaultValue = "10") Integer pageSize){
//        User user = (User) session.getAttribute(Const.CURRENT_USER);

//        //二期代码
//        String loginToken = CookieUtil.readLoginToken(httpServletRequest);
//        if(StringUtils.isEmpty(loginToken)){
//            return ServerResponse.createByErrorMessage("用户未登录,无法获取当前用户信息");
//        }
//        String userJsonStr = RedisShardedPoolUtil.get(loginToken);
//        User user = JsonUtil.string2Obj(userJsonStr,User.class);
//        if(user ==null){
//            return ServerResponse.createByErrorCodeMessage(ResponseCode.NEED_LOGIN.getCode(),"用户未登录，请登录");
//        }
//        if(iUserService.checkAdminRole(user).isSuccess()){
//            //填充业务
//            return iProductService.manageSearchProduct(productName,productId,pageNum,pageSize);
//        }else{
//            return ServerResponse.createByErrorMessage("无权限操作，需要管理员权限");
//        }

        return iProductService.manageSearchProduct(productName,productId,pageNum,pageSize);

    }

    @RequestMapping(value = "upload.do")
    @ResponseBody
    //文件上传接口
    public ServerResponse upload(HttpServletRequest httpServletRequest,
                                 @RequestParam(value = "upload_file" ,required = false) MultipartFile file, HttpServletRequest request){

//        User user = (User) session.getAttribute(Const.CURRENT_USER);

        //二期代码
//        String loginToken = CookieUtil.readLoginToken(httpServletRequest);
//        if(StringUtils.isEmpty(loginToken)){
//            return ServerResponse.createByErrorMessage("用户未登录,无法获取当前用户信息");
//        }
//        String userJsonStr = RedisShardedPoolUtil.get(loginToken);
//        User user = JsonUtil.string2Obj(userJsonStr,User.class);
//        if(user ==null){
//            return ServerResponse.createByErrorCodeMessage(ResponseCode.NEED_LOGIN.getCode(),"用户未登录，请登录");
//        }
//        if(iUserService.checkAdminRole(user).isSuccess()){
//            //填充业务
//            //从request session中拿到servlet上下文，getRealPath获取上传的文件夹
//            String path = request.getSession().getServletContext().getRealPath("upload");
//            String targetFileName = iFileService.upload(file,path);
//            String url = PropertiesUtil.getProperty("ftp.server.http.prefix")+targetFileName;
//
////            Map fileMap = Maps.newHashMap();
////            //Map 映射情况：键值对
////            fileMap.put("uri",targetFileName);
////            fileMap.put("url",url);
////            return ServerResponse.createBySuccess(fileMap);
//            Map fileMap = Maps.newHashMap();
//            fileMap.put("uri",targetFileName);
//            fileMap.put("url",url);
//            return ServerResponse.createBySuccess(fileMap);
//        }else{
//            return ServerResponse.createByErrorMessage("无权限操作，需要管理员权限");
//        }

        String path = request.getSession().getServletContext().getRealPath("upload");
        String targetFileName = iFileService.upload(file,path);
        String url = PropertiesUtil.getProperty("ftp.server.http.prefix")+targetFileName;

//            Map fileMap = Maps.newHashMap();
//            //Map 映射情况：键值对
//            fileMap.put("uri",targetFileName);
//            fileMap.put("url",url);
//            return ServerResponse.createBySuccess(fileMap);
        Map fileMap = Maps.newHashMap();
        fileMap.put("uri",targetFileName);
        fileMap.put("url",url);
        return ServerResponse.createBySuccess(fileMap);


    }


    @RequestMapping(value = "richtext_img_upload.do")
    @ResponseBody
    //富文件上传图片
    public Map richTextImgUpload( @RequestParam(value = "upload_file" ,required = false) MultipartFile file,
                                 HttpServletRequest request, HttpServletResponse response){
        Map resultMap = Maps.newHashMap();
//        User user = (User) session.getAttribute(Const.CURRENT_USER);
        //二期代码
//        String loginToken = CookieUtil.readLoginToken(request);
//        if(StringUtils.isEmpty(loginToken)){
//            resultMap.put("succsess",false);
//            resultMap.put("msg","请登录管理员");
//            return resultMap;
//        }
//        String userJsonStr = RedisShardedPoolUtil.get(loginToken);
//        User user = JsonUtil.string2Obj(userJsonStr,User.class);
//        if(user ==null){
//            resultMap.put("succsess",false);
//            resultMap.put("msg","请登录管理员");
//            return resultMap;
//        }
//
//        //富文本中对于返回值有自己的要求，我们使用的是simditor，所以要按照simditor的要求返回
//        //simditor对图片上传接口的格式要求如下:
//        //{
//        // "success": true/false,
//        // "msg": "error message", # optional
//        // "file_path": "[real file path]"
//        // }
//        if(iUserService.checkAdminRole(user).isSuccess()){
//            //填充业务
//            //从request session中拿到servlet上下文，getRealPath获取上传的文件夹
//            String path = request.getSession().getServletContext().getRealPath("upload");
//            String targetFileName = iFileService.upload(file,path);
//            if(StringUtils.isBlank(targetFileName)){
//                resultMap.put("succsess",false);
//                resultMap.put("msg","上传失败");
//                return resultMap;
//            }
//            String url = PropertiesUtil.getProperty("ftp.server.http.prefix")+targetFileName;
//            resultMap.put("succsess",true);
//            resultMap.put("msg","上传成功");
//            resultMap.put("file_path",url);
//            response.addHeader("Access-Control-Allow-Headers","X-File-Name");
//            return resultMap;
//        }else{
//            resultMap.put("succsess",false);
//            resultMap.put("msg","无权限操作");
//            return resultMap;
//        }

        //从request session中拿到servlet上下文，getRealPath获取上传的文件夹
        String path = request.getSession().getServletContext().getRealPath("upload");
        String targetFileName = iFileService.upload(file,path);
        if(StringUtils.isBlank(targetFileName)){
            resultMap.put("succsess",false);
            resultMap.put("msg","上传失败");
            return resultMap;
        }
        String url = PropertiesUtil.getProperty("ftp.server.http.prefix")+targetFileName;
        resultMap.put("succsess",true);
        resultMap.put("msg","上传成功");
        resultMap.put("file_path",url);
        response.addHeader("Access-Control-Allow-Headers","X-File-Name");
        return resultMap;
    }










}
