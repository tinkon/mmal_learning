package com.mmall.util;

import com.mmall.common.RedisPool;
import lombok.extern.slf4j.Slf4j;
import redis.clients.jedis.Jedis;

@Slf4j
public class RedisPoolUtil {

    //重新设置key的有效期的开放方法
    public static Long expire(String key,Integer exTime){
        Jedis jedis = null;
        Long result = null;

        try {
            jedis = RedisPool.getJedis();//获取连接
            result = jedis.expire(key,exTime);
        }catch (Exception e){
            log.error("set key:{},erro",key,e);
            RedisPool.returnBrokenResource(jedis);
            return result;
        }
        RedisPool.retrunResource(jedis);
        return result;
    }

    //exTime的单位是秒，设置键的有效时间开放方法
    public static String setExTime(String key,String value,Integer exTime){
        Jedis jedis = null;
        String result = null;

        try {
            jedis = RedisPool.getJedis();//获取连接
            result = jedis.setex(key,exTime,value);
        }catch (Exception e){
            log.error("set key:{},value:{},erro",key,value,e);
            RedisPool.returnBrokenResource(jedis);
            return result;
        }
        RedisPool.retrunResource(jedis);
        return result;
    }

    //设置键值开放方法
    public static String set(String key,String value){
        Jedis jedis = null;
        String result = null;

        try {
            jedis = RedisPool.getJedis();//获取连接
            result = jedis.set(key,value);
        }catch (Exception e){
            log.error("set key:{},value:{},erro",key,value,e);
            RedisPool.returnBrokenResource(jedis);
            return result;
        }
        RedisPool.retrunResource(jedis);
        return result;
    }

    //根据键获取value开放方法
    public static String get(String key){
        Jedis jedis = null;
        String result = null;
        try {
            jedis = RedisPool.getJedis();//获取连接
            result = jedis.get(key);
        }catch (Exception e){
            log.error("get key:{},erro",key,e);
            RedisPool.returnBrokenResource(jedis);
            return result;
        }
        RedisPool.retrunResource(jedis);
        return result;
    }

    //根据键key删除
    public static Long del(String key){
        Jedis jedis = null;
        Long result = null;
        try {
            jedis = RedisPool.getJedis();//获取连接
            result = jedis.del(key);
        }catch (Exception e){
            log.error("get key:{},erro",key,e);
            RedisPool.returnBrokenResource(jedis);
            return result;
        }
        RedisPool.retrunResource(jedis);
        return result;
    }


    public static void main(String[] args) {
        Jedis jedis = RedisPool.getJedis();

        RedisShardedPoolUtil.set("keytest","keyvalue");
        String value = RedisShardedPoolUtil.get("keytest");

        RedisShardedPoolUtil.setExTime("keyex","keyexvalue",60);
        RedisShardedPoolUtil.expire("keytest",60);
        RedisShardedPoolUtil.del("keytest");
        System.out.println("end");
    }

}
