package com.mmall.util;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Slf4j
public class CookieUtil {

    private final static String COOKIE_DOMAIN = "tinkon.com";
    private final static String COOKIE_NAME = "mmall_login_tokon";


    public static String readLoginToken(HttpServletRequest request){
        Cookie[] cookies = request.getCookies();
        if(cookies != null){
            for (Cookie ck : cookies){
                log.info("read cookieName:{},cookieValue:{}",ck.getName(),ck.getValue());
                if(StringUtils.equals(ck.getName(),COOKIE_NAME)){
                    log.info("return cookieName:{},cookieValue:{}",ck.getName(),ck.getValue());
                    return ck.getValue();
                }
            }
        }
        return null;
    }

    //a:A.tinkon.com                cookie:domain=A.tinkon.com;path="/"
    //b:B.tinkon.com                cookie:domain=B.tinkon.com;path="/"
    //c:A.tinkon.com/test/cc        cookie:domain=A.tinkon.com;path="/test/cc"
    //d:A.tinkon.com/test/dd        cookie:domain=B.tinkon.com;path="/test/dd"
    //e:A.tinkon.com/test/

    public static void writeLoginToken(HttpServletResponse response,String token){

        Cookie ck = new Cookie(COOKIE_NAME,token);
        ck.setDomain(COOKIE_DOMAIN);
        ck.setPath("/");//代表设置在根目录
        ck.setHttpOnly(true);
        //如果MaxAge不设置，cookie不会写入硬盘仅写在内存，只在当前界面有效。
        ck.setMaxAge(60 * 60 *24 *365);//-1代表永久，0为删除，现在设置为一年的有效期
        log.info("write cookieName:{},cookieValue:{}",ck.getName(),ck.getValue());
        response.addCookie(ck);
    }



    public static void delLoginToken(HttpServletRequest request,HttpServletResponse response){
        Cookie[] cookies = request.getCookies();
        if(cookies != null){
            for (Cookie ck : cookies){
                if(StringUtils.equals(ck.getName(),COOKIE_NAME)){
                    ck.setDomain(COOKIE_DOMAIN);
                    ck.setPath("/");
                    ck.setMaxAge(0);//0代表删除cookie
                    log.info("del cookieName:{},cookieValue:{}",ck.getName(),ck.getValue());
                    response.addCookie(ck);
                    return;
                }
            }
        }

    }

}
