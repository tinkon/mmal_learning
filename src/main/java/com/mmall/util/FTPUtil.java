package com.mmall.util;


import lombok.extern.slf4j.Slf4j;
import org.apache.commons.net.ftp.FTPClient;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;

@Slf4j
public class FTPUtil {

//    private static final Logger logger = LoggerFactory.getLogger(FTPUtil.class);

    private static String ftpIp =PropertiesUtil.getProperty("ftp.server.ip");
    private static String ftpUser =PropertiesUtil.getProperty("ftp.user");
    private static String ftpPassword =PropertiesUtil.getProperty("ftp.pass");

    private String ip;
    private int port;
    private String user;
    private  String pwd;
    private FTPClient ftpClient;

    public FTPUtil(String ip,int port,String user,String pwd){
        this.ip = ip;
        this.port = port;
        this.user = user;
        this.pwd = pwd;
    }

    //对外开放的方法。uploadFile
    public  static  boolean uploadFile(List<File> fileList) throws IOException {
        FTPUtil ftpUtil = new FTPUtil(ftpIp,21,ftpUser,ftpPassword);
        log.info("开始连接ftp服务器");
        boolean result =ftpUtil.privateuploadFile("img",fileList);
        log.info("开始连接ftp服务器，结束上传，上传结果：{}",result);

        return result;
    }
    //封装上传图片逻辑
    private boolean  privateuploadFile(String remotePath,List<File> fileList) throws IOException {
        boolean uploaded =true;
        FileInputStream fileInputStream = null;
        //连接ftp服务器 连接ftp利用封装方法
        if(connectServer(this.ip,this.port,this.user,this.pwd)){
            try {
//                连接ftp成功
                ftpClient.changeWorkingDirectory(remotePath);//改变工作目录
                ftpClient.setBufferSize(1024);//设置缓存
                ftpClient.setControlEncoding("UTF-8");
                ftpClient.setFileType(FTPClient.BINARY_FILE_TYPE);//将文件设置成二进制模式
                ftpClient.enterLocalPassiveMode();//打开本地的被动模式
                for(File fileItem:fileList){
                    fileInputStream = new FileInputStream(fileItem);
                    ftpClient.storeFile(fileItem.getName(),fileInputStream);//存储文件
                    log.info("存储文件完成");
                }
            } catch (IOException e) {
                log.error("上传文件异常",e);
                uploaded =false;
//                e.printStackTrace();
            }finally {
                fileInputStream.close();
                ftpClient.disconnect();
            }
        }
        return uploaded;
    }
    //封装连接ftp服务器方法
    private boolean connectServer(String ip,int port,String user,String pwd){
        boolean isSuccess = false;
        ftpClient =new FTPClient();
        try {
            //连接端口
            ftpClient.connect(ip);
            log.info("连接端口:{}",ip);
            //登录ftp服务器
            isSuccess = ftpClient.login(user, pwd);
            log.info("登录ftp服务器结果:{}",isSuccess);
        } catch (IOException e) {
            log.error("连接服务器异常", e);
        }
        return isSuccess;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public FTPClient getFtpClient() {
        return ftpClient;
    }

    public void setFtpClient(FTPClient ftpClient) {
        this.ftpClient = ftpClient;
    }
}
