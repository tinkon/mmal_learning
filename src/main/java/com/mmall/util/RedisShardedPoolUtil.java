package com.mmall.util;

import com.mmall.common.RedisShardPool;
import lombok.extern.slf4j.Slf4j;
import redis.clients.jedis.ShardedJedis;

@Slf4j
public class RedisShardedPoolUtil {

    //设置键值开放方法
    public static String set(String key,String value){
        ShardedJedis shardedJedis = null;
        String result = null;
        try {
            shardedJedis = RedisShardPool.getShardedJedis();
            result = shardedJedis.set(key,value);
        }catch (Exception e){
            log.error("set key:{},value:{},erro",key,value,e);
            RedisShardPool.returnBrokenResource(shardedJedis);
            return result;
        }
        RedisShardPool.returnResource(shardedJedis);
        return result;
    }

    public static Long setNx(String key,String value){
        ShardedJedis shardedJedis = null;
        Long result = null;
        try {
            shardedJedis = RedisShardPool.getShardedJedis();
            result = shardedJedis.setnx(key,value);
        }catch (Exception e){
            log.error("setnx key:{},value:{},erro",key,value,e);
            RedisShardPool.returnBrokenResource(shardedJedis);
            return result;
        }
        RedisShardPool.returnResource(shardedJedis);
        return result;
    }

    public static String getSet(String key,String value){
        ShardedJedis shardedJedis = null;
        String result = null;
        try {
            shardedJedis = RedisShardPool.getShardedJedis();
            result = shardedJedis.getSet(key,value);
        }catch (Exception e){
            log.error("getSet key:{},value:{},erro",key,value,e);
            RedisShardPool.returnBrokenResource(shardedJedis);
            return result;
        }
        RedisShardPool.returnResource(shardedJedis);
        return result;
    }


    //exTime的单位是秒，设置键的有效时间开放方法
    public static String setExTime(String key,String value,int exTime){
        ShardedJedis shardedJedis = null;
        String result = null;
        try {
            shardedJedis = RedisShardPool.getShardedJedis();
            result = shardedJedis.setex(key,exTime,value);
        }catch (Exception e){
            log.error("set key:{},value:{},erro",key,value,e);
            RedisShardPool.returnBrokenResource(shardedJedis);
            return result;
        }
        RedisShardPool.returnResource(shardedJedis);
        return result;
    }

    //重新设置key的有效期的开放方法
    public static Long expire(String key,int exTime){
        ShardedJedis shardedJedis = null;
        Long result = null;
        try {
            shardedJedis = RedisShardPool.getShardedJedis();
            result = shardedJedis.expire(key,exTime);
        }catch (Exception e){
            log.error("set key:{},erro",key,e);
            RedisShardPool.returnBrokenResource(shardedJedis);
            return result;
        }
        RedisShardPool.returnResource(shardedJedis);
        return result;
    }

    //根据键获取value开放方法
    public static String get(String key){
        ShardedJedis shardedJedis = null;
        String result = null;
        try {
            shardedJedis = RedisShardPool.getShardedJedis();//获取连接
            result = shardedJedis.get(key);
        }catch (Exception e){
            log.error("get key:{},erro",key,e);
            RedisShardPool.returnBrokenResource(shardedJedis);
            return result;
        }
        RedisShardPool.returnResource(shardedJedis);
        return result;
    }

    //根据键key删除
    public static Long del(String key){
        ShardedJedis shardedJedis = null;
        Long result = null;
        try {
            shardedJedis = RedisShardPool.getShardedJedis();//获取连接
            result = shardedJedis.del(key);
        }catch (Exception e){
            log.error("get key:{},erro",key,e);
            RedisShardPool.returnBrokenResource(shardedJedis);
            return result;
        }
        RedisShardPool.returnResource(shardedJedis);
        return result;
    }

}
