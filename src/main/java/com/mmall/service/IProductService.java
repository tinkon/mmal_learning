package com.mmall.service;

import com.github.pagehelper.PageInfo;
import com.mmall.common.ServerResponse;
import com.mmall.pojo.Product;
import com.mmall.vo.ProductDetailVo;

public  interface IProductService {
    ServerResponse manageSaveOrUpdateProduct(Product product);

    ServerResponse<String> manageSetSaleStatus(Integer productId,Integer status);

    ServerResponse<ProductDetailVo> manageProductDetail(Integer productId);

    ServerResponse<PageInfo> manageGetProductList(Integer pageNum,Integer pageSize);

    ServerResponse<PageInfo> manageSearchProduct(String productName, Integer productId, Integer pageNum, Integer pageSize);

    ServerResponse<ProductDetailVo> getProductDetail(Integer productId);

    ServerResponse<PageInfo> getProductByKeywordAndCatrgoryId(String keyword, Integer categoryId, Integer pageNum,Integer pageSize,String orderby);
}
