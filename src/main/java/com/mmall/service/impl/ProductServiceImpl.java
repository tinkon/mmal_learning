package com.mmall.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.google.common.collect.Lists;
import com.mmall.common.Const;
import com.mmall.common.ResponseCode;
import com.mmall.common.ServerResponse;
import com.mmall.dao.CategoryMapper;
import com.mmall.dao.ProductMapper;
import com.mmall.pojo.Category;
import com.mmall.pojo.Product;
import com.mmall.service.ICategoryService;
import com.mmall.service.IProductService;
import com.mmall.util.DateTimeUtil;
import com.mmall.util.PropertiesUtil;
import com.mmall.vo.ProductDetailVo;
import com.mmall.vo.ProductListVo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service("iProductService")
public class ProductServiceImpl implements IProductService {

    @Autowired
    private ProductMapper productMapper;
    @Autowired
    private CategoryMapper categoryMapper;
    @Autowired
    private ICategoryService iCategoryService;

    //    保存或者更新商品
    public ServerResponse manageSaveOrUpdateProduct(Product product){
        if (product != null){
            if(StringUtils.isNotBlank(product.getSubImages())){
                //子图获取分隔，放入字符串subImageArray变量中
                String[] subImageArray = product.getSubImages().split(",");
                if (subImageArray.length>0){
                    //将子图的首张传递给mainImage
                    product.setMainImage(subImageArray[0]);
                }
             }

             if(product.getId() != null){
                 //表明商品表中已经有该记录，进行更新操作
                int rowCount =  productMapper.updateByPrimaryKey(product);
                if(rowCount > 0){
                    return ServerResponse.createBySuccessMessage("更新产品成功");
                }
                 return ServerResponse.createBySuccessMessage("更新产品失败");
             }else {
                 //没有，新增商品到商品表中
                 int rowCount =   productMapper.insert(product);
                 if (rowCount > 0){
                     return ServerResponse.createBySuccessMessage("新增产品成功");
                 }
                 return ServerResponse.createBySuccessMessage("新增产品失败");
             }
        }else {
            return ServerResponse.createByErrorMessage("新增或更新产品参数不正确");
        }
    }

    //    管理商品上下架状态
    public ServerResponse<String> manageSetSaleStatus(Integer productId,Integer status){
        if(productId == null||status == null){
            return ServerResponse.createByErrorCodeMessage(ResponseCode.ILLEGAL_ARGUMENT.getCode(),ResponseCode.ILLEGAL_ARGUMENT.getDesc());
        }
        Product product = new Product();
        product.setId(productId);
        product.setStatus(status);
        int rowCount = productMapper.updateByPrimaryKeySelective(product);
        if (rowCount > 0){
            return ServerResponse.createBySuccessMessage("更新产品状态成功");
        }
        return ServerResponse.createByErrorMessage("更新产品状态失败");
    }

    //    后台获取商品详情
    public ServerResponse<ProductDetailVo> manageProductDetail(Integer productId){
        if(productId == null){
            return ServerResponse.createByErrorCodeMessage(ResponseCode.ILLEGAL_ARGUMENT.getCode(),ResponseCode.ILLEGAL_ARGUMENT.getDesc());
        }

        Product product = productMapper.selectByPrimaryKey(productId);
        if(product == null){
            return ServerResponse.createByErrorMessage("产品已下架或删除");
        }
        //vo对象--》 value object
        //pojo---》bo（business object）--》vo
        ProductDetailVo productDetailVo = assembleProductDetailVo(product);
        return ServerResponse.createBySuccess(productDetailVo);

    }

    //为了使用这个VO，我们不能在其内部去写方法，然后在外部调用，这样是不符合情理的。
    // 对于这种情况，可以在对应的Service中封装一个处理VO的方法，
    // 这样一来，就可以实现“点对点”的功能服务。在该方法中，写一个处理商品详情的VO方法
    // 用户返回给前端一个商品详情类，那么为什么要用到这个封装类呢？因为我们返回给前端的不仅仅是一个数据表里面的信息，
    // 我们返回的多个表综合的信息。所以使用的是多表查询，我们自己封装了一个ProductDetailVo类，来定义我们需要返回给前端定义的字段。。
    private ProductDetailVo assembleProductDetailVo(Product product){
        ProductDetailVo productDetailVo = new ProductDetailVo();
        productDetailVo.setId(product.getId());
        productDetailVo.setSubtitle(product.getSubtitle());
        productDetailVo.setPrice(product.getPrice());
        productDetailVo.setMainImage(product.getMainImage());
        productDetailVo.setSubImages(product.getSubImages());
        productDetailVo.setCategoryId(product.getCategoryId());
        productDetailVo.setDetail(product.getDetail());
        productDetailVo.setName(product.getName());
        productDetailVo.setStatus(product.getStatus());
        productDetailVo.setStock(product.getStock());

        productDetailVo.setImageHost(PropertiesUtil.getProperty("ftp.server.http.prefix","http://image.imooc.com/"));

        //根据商品种类id查找商品种类表
        Category category = categoryMapper.selectByPrimaryKey(product.getCategoryId());
        if(category == null){
            //为空，商品种类表中无此分类，故设置为0根节点
            productDetailVo.setParentCategoryId(0);//默认根节点
        }else{
            //根据商品种类表中的父节点设置(获取商品种类表中的parentId)
            productDetailVo.setParentCategoryId(category.getParentId());
        }

        productDetailVo.setCreateTime(DateTimeUtil.dateToStr(product.getCreateTime()));
        productDetailVo.setUpdateTime(DateTimeUtil.dateToStr(product.getUpdateTime()));
        return productDetailVo;
    }

    //查询商品list
    public ServerResponse<PageInfo> manageGetProductList(Integer pageNum,Integer pageSize){
        //1.startPage--->start
        //2.填充自己的sql查询逻辑
        //3.pageHelper -->收尾
        PageHelper.startPage(pageNum,pageSize);
        List<Product> productList = productMapper.selectList();

        List<ProductListVo> productListVoList = Lists.newArrayList();
        for(Product productItem : productList){
            ProductListVo productListVo = assembleProductListVo(productItem);
            productListVoList.add(productListVo);
        }
        PageInfo pageInfo = new PageInfo(productList);
        pageInfo.setList(productListVoList);
        return ServerResponse.createBySuccess(pageInfo);
    }

    //封装返回给前端的商品列表信息Vo类 (组装ProductListVo对象)
    private ProductListVo assembleProductListVo(Product product){
        ProductListVo productListVo = new ProductListVo();
        productListVo.setId(product.getId());
        productListVo.setName(product.getName());
        productListVo.setCategoryId(product.getCategoryId());
        productListVo.setImageHost(PropertiesUtil.getProperty("ftp.server.http.prefix","http://image.imooc.com/"));
        productListVo.setMainImage(product.getMainImage());
        productListVo.setPrice(product.getPrice());
        productListVo.setSubtitle(product.getSubtitle());
        productListVo.setStatus(product.getStatus());
        return productListVo;


    }

    //    商品搜索
    public ServerResponse<PageInfo> manageSearchProduct(String productName,Integer productId,Integer pageNum,Integer pageSize){
       //分页
        PageHelper.startPage(pageNum,pageSize );
        if(StringUtils.isNotBlank(productName)){
            //转换为sql查询用%小明%
            productName = new StringBuilder().append("%").append(productName).append("%").toString();
        }
        List<Product> productList = productMapper.selectByNameAndProductId(productName,productId);
        List<ProductListVo> productListVoList = Lists.newArrayList();
        for(Product productItem:productList){
            ProductListVo productListVo = assembleProductListVo(productItem);
            productListVoList.add(productListVo);
        }
        PageInfo pageInfo = new PageInfo(productList);
        pageInfo.setList(productListVoList);
        return ServerResponse.createBySuccess(pageInfo);
    }


    public ServerResponse<ProductDetailVo> getProductDetail(Integer productId){
        if(productId == null){
            return ServerResponse.createByErrorCodeMessage(ResponseCode.ILLEGAL_ARGUMENT.getCode(),ResponseCode.ILLEGAL_ARGUMENT.getDesc());
        }
        Product product = productMapper.selectByPrimaryKey(productId);
        if(product == null){
            return ServerResponse.createByErrorMessage("产品已下架或者删除");
        }
        //判断product的在售状态，ON_SALE=1为在售状态
        if(product.getStatus() != Const.ProductStatusEnum.ON_SALE.getCode()){
            return ServerResponse.createByErrorMessage("产品已下架或者删除");

        }
        ProductDetailVo  productDetailVo = assembleProductDetailVo(product);
        return ServerResponse.createBySuccess(productDetailVo);
    }


    public ServerResponse<PageInfo> getProductByKeywordAndCatrgoryId(String keyword, Integer categoryId, Integer pageNum,Integer pageSize,String orderby){
        if(StringUtils.isBlank(keyword) && categoryId == null){
            return ServerResponse.createByErrorCodeMessage(ResponseCode.ILLEGAL_ARGUMENT.getCode(),ResponseCode.ILLEGAL_ARGUMENT.getDesc());
            }
         List<Integer> categoryIdList = new ArrayList<Integer>();
        if(categoryId != null){
            Category category = categoryMapper.selectByPrimaryKey(categoryId);
            if(category == null && StringUtils.isBlank(keyword)){
                //没有该分类，并且没有关键字，这个时候返回一个空结果集
                PageHelper.startPage(pageNum,pageSize );
                List<ProductDetailVo> productDetailVoList = Lists.newArrayList();
                PageInfo pageInfo = new PageInfo(productDetailVoList);
                return ServerResponse.createBySuccess(pageInfo);
            }
            categoryIdList = iCategoryService.selectCategoryAndChildrenById(category.getId()).getData();
        }
        if(StringUtils.isNotBlank(keyword)){
            keyword = new StringBuilder().append("%").append(keyword).append("%").toString();
        }
        PageHelper.startPage(pageNum,pageSize);
        //排序处理
        if(StringUtils.isNotBlank(orderby)){
            if(Const.ProductListOrderBy.PRICE_ASC_DESC.contains(orderby)){
                String[] orderByArray = orderby.split("_");
                PageHelper.orderBy(orderByArray[0]+" "+orderByArray[1]);//price desc或者price asc
            }
        }
        List<Product> productList = productMapper.selectByNameAndCategoryIds(StringUtils.isBlank(keyword)?null:keyword,categoryIdList.size() ==0?null:categoryIdList);

        List<ProductListVo> productListVoList = Lists.newArrayList();
        for (Product productItem:productList){
            ProductListVo productListVo = assembleProductListVo(productItem);
            productListVoList.add(productListVo);
        }
        PageInfo pageInfo = new PageInfo(productList);
        pageInfo.setList(productListVoList);
        return ServerResponse.createBySuccess(pageInfo);
    }


}
