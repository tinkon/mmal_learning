package com.mmall.service.impl;

import com.google.common.base.Splitter;
import com.google.common.collect.Lists;
import com.mmall.common.Const;
import com.mmall.common.ResponseCode;
import com.mmall.common.ServerResponse;
import com.mmall.dao.CartMapper;
import com.mmall.dao.ProductMapper;
import com.mmall.pojo.Cart;
import com.mmall.pojo.Product;
import com.mmall.service.ICartService;
import com.mmall.util.BigDecimalUtil;
import com.mmall.util.PropertiesUtil;
import com.mmall.vo.CartProductVo;
import com.mmall.vo.CartVo;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

@Service("iCartService")
public class CartServiceImpl implements ICartService {

    @Autowired
    private CartMapper cartMapper;

    @Autowired
    private ProductMapper productMapper;

    /**
     * @param userId
     * @param productId
     * @param count
     * @return
     */
    public ServerResponse<CartVo> add( Integer userId,Integer productId, Integer count){

        if(productId == null || count == null){
            return ServerResponse.createByErrorCodeMessage(ResponseCode.ILLEGAL_ARGUMENT.getCode(),ResponseCode.ILLEGAL_ARGUMENT.getDesc());
        }

        Cart cart = cartMapper.selectCartByUserIdProductId(userId,productId);//传入用户和商品的id查找购物车表中的数据
                if(cart == null){
                    //这个产品不在用户的购物车里，需要新增一个产品的记录
                    //cart为0说明购物车表中没有这条记录
                    Cart cartItem = new Cart();//创建一个购物车新对象
                    cartItem.setQuantity(count);//创建数量
                    cartItem.setChecked(Const.CartCheck.CHECKED);//创建勾选状态
                    cartItem.setProductId(productId);//创建商品的id
                    cartItem.setUserId(userId);//创建用户的id
                    cartMapper.insert(cartItem);//插入到购物车表中
                }else {
                    //购物车表中已经有这条记录，使其数量相加
                    count = cart.getQuantity() + count;
                    cart.setQuantity(count);
                    cartMapper.updateByPrimaryKeySelective(cart);//更新到购物车表中
                }
//        CartVo cartVo = this.getCartVoLimit(userId);
//        return ServerResponse.createBySuccess(cartVo);
        //封装好了list展示列表，所以可以直接调用
        return this.list(userId);
    }


    public ServerResponse<CartVo> update(Integer userId,Integer productId, Integer count){
        if(productId == null || count == null){
            return ServerResponse.createByErrorCodeMessage(ResponseCode.ILLEGAL_ARGUMENT.getCode(),ResponseCode.ILLEGAL_ARGUMENT.getDesc());
        }
        Cart cart = cartMapper.selectCartByUserIdProductId(userId,productId);
        if (cart != null){
            cart.setQuantity(count);
        }
        cartMapper.updateByPrimaryKeySelective(cart);
//        CartVo cartVo = this.getCartVoLimit(userId);
//        return ServerResponse.createBySuccess(cartVo);
        return this.list(userId);
    }

    public ServerResponse<CartVo> delete(Integer userId,String productIds){
        List<String> productList = Splitter.on(",").splitToList(productIds);

        if(CollectionUtils.isEmpty(productList)){
            return ServerResponse.createByErrorCodeMessage(ResponseCode.ILLEGAL_ARGUMENT.getCode(),ResponseCode.ILLEGAL_ARGUMENT.getDesc());
        }
        cartMapper.deleteProduct(userId,productList);
//        CartVo cartVo = this.getCartVoLimit(userId);
//        return ServerResponse.createBySuccess(cartVo);
        return this.list(userId);
    }

    public ServerResponse<CartVo> list(Integer userId){
        CartVo cartVo = this.getCartVoLimit(userId);
        return ServerResponse.createBySuccess(cartVo);
    }

    public ServerResponse<CartVo> selectOrUnSelect(Integer userId,Integer productId,Integer checked){
        cartMapper.checkedOrUncheckedProduct(userId,productId,checked);
        return this.list(userId);

    }

    public ServerResponse<Integer> getCartProductCount(Integer userId){
        if(userId == null){
            return ServerResponse.createBySuccess(0);
        }
        int rowCount = cartMapper.selectCartProductCount(userId);
        return ServerResponse.createBySuccess(rowCount);
    }


    //库存和购物车数量做一个限制，不能让购物车的数量比库存还多
    private CartVo getCartVoLimit(Integer userId){
        CartVo cartVo = new CartVo();
        //获取pojo对象，根据userId查一个购物车的集合
        List<Cart> cartList = cartMapper.selectCartByUserId(userId);
        List<CartProductVo> cartProductVolist = Lists.newArrayList();

        BigDecimal cartTotalPrice = new BigDecimal("0");

        if (CollectionUtils.isNotEmpty(cartList)){
            //传入用户的id来查找购物车集合，不为空证明购物车表中是有记录的
            for(Cart cartItem : cartList){
                //foreach遍历查找出来的购物车表中的记录，组装对应的CartProductVo对象
                CartProductVo cartProductVo = new CartProductVo();
                cartProductVo.setId(cartItem.getId());
                cartProductVo.setUserId(cartItem.getUserId());
                cartProductVo.setProductId(cartItem.getProductId());

                //根据购物车中的productId查找商品的对象
                Product product = productMapper.selectByPrimaryKey(cartItem.getProductId());
                if(product != null){
                    //传入购物车中productId来查找商品对象，不为空证明商品表中是有该商品的记录，继续组装CartProductVo对象
                    cartProductVo.setProductMainImage(product.getMainImage());
                    cartProductVo.setProductName(product.getName());
                    cartProductVo.setProductSubtitle(product.getSubtitle());
                    cartProductVo.setProductStatus(product.getStatus());
                    cartProductVo.setProductPrice(product.getPrice());
                    cartProductVo.setProductStock(product.getStock());
                    //判断库存  很重要
                    int buyLimitCount = 0;
                    if(product.getStock() >= cartItem.getQuantity()){
                        //库存充足
                        buyLimitCount = cartItem.getQuantity();
                        cartProductVo.setLimitQuantity(Const.CartCheck.LIMIT_NUM_SUCCESS);//设置Limit为success状态
                    }else {
                        buyLimitCount = product.getStock();
                        cartProductVo.setLimitQuantity(Const.CartCheck.LIMIT_NUM_FAIL);//设置Limit为fail状态
                        //购物车更新有效库存
                        Cart cartForQuantity = new Cart();
                        cartForQuantity.setId(cartItem.getId());
                        cartForQuantity.setQuantity(buyLimitCount);
                        cartMapper.updateByPrimaryKeySelective(cartForQuantity);//更新购物车中的数量状态
                    }
                    cartProductVo.setQuantity(buyLimitCount);
                    //计算购物车中商品总价
                    cartProductVo.setProductTotalPrice(BigDecimalUtil.mul(product.getPrice().doubleValue(),cartProductVo.getQuantity()));
                    cartProductVo.setProductChecked(cartItem.getChecked());
                }
                if(cartItem.getChecked() == Const.CartCheck.CHECKED){
                    //如果已经勾选，增加到整个购物车总价中
                    cartTotalPrice  = BigDecimalUtil.add(cartTotalPrice.doubleValue(),cartProductVo.getProductTotalPrice().doubleValue());
                }
                cartProductVolist.add(cartProductVo);//放到cartProductVo集合中
            }
        }
        //组装cartVo对象并返回
        cartVo.setCartTotalPrice(cartTotalPrice);
        cartVo.setCartProductVoList(cartProductVolist);
        cartVo.setAllChecked(this.getAllCheckedStatus(userId));
        cartVo.setImgHost(PropertiesUtil.getProperty("ftp.server.http.prefix"));
        return cartVo;
    }

    private boolean getAllCheckedStatus(Integer userId){
        if(userId == null){
            return false;
        }
        return cartMapper.selectCartProductCheckedStatusByUserId(userId) == 0;

    }

















}
