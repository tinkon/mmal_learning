package com.mmall.service.impl;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.mmall.common.ServerResponse;
import com.mmall.dao.CategoryMapper;
import com.mmall.pojo.Category;
import com.mmall.service.ICategoryService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Set;

@Service("iCategoryService")
public class CategoryServiceImpl implements ICategoryService {

    private Logger logger = LoggerFactory.getLogger(CategoryServiceImpl.class);

    @Autowired
    private CategoryMapper categoryMapper;

    //首先校验前端传过来的categoryName和parentId是否存在，如果不存在则提示参数错误，否则就继续使用JavaBean的实例来增加品类。
    // 同样的，在用JavaBean增加完之后，将结果插入到数据库中，如果返回的生效行数大于0，则添加品类成功，否则添加品类失败
    public ServerResponse addCategory(String categoryName,Integer parentId){
        if(parentId ==null|| StringUtils.isBlank(categoryName)){
            return ServerResponse.createByErrorMessage("添加品类参数错误");
        }
        Category category =new Category();
        category.setName(categoryName);
        category.setParentId(parentId);
        category.setStatus(true);

        int rowCount = categoryMapper.insert(category);
        if (rowCount>0){
            return ServerResponse.createBySuccessMessage("添加品类成功");
        }
        return ServerResponse.createByErrorMessage("添加品类失败");
    }

    //首先校验前端传过来的categoryName和catrgoryId是否存在，如果不存在则提示参数错误，否则就继续使用JavaBean的实例来更新品类。
    // 同样的，在用JavaBean更新完之后，将结果更新到数据库中，如果返回的生效行数大于0，则更新品类成功，否则更新品类失败
    public ServerResponse updateCategoryName(Integer catrgoryId,String categoryName){
        if(catrgoryId ==null|| StringUtils.isBlank(categoryName)){
            return ServerResponse.createByErrorMessage("更新品类参数错误");
        }
        Category category =new Category();
        category.setId(catrgoryId);
        category.setName(categoryName);

        int rowCount = categoryMapper.updateByPrimaryKeySelective(category);
        if(rowCount>0){
            return ServerResponse.createBySuccessMessage("更新品类名称成功");
        }else {
            return ServerResponse.createByErrorMessage("更新品类名称失败");
        }
    }

    //获取平级品类结点（后台商品搜索）功能的实现
    public ServerResponse<List<Category>> getChildrenParallelCategory(Integer categoryId){

        List<Category> categoryList = categoryMapper.selectCategoryChildrenByParentId(categoryId);
        //集合判断为空
        if(CollectionUtils.isEmpty(categoryList)){
            //使用Java中封装好的CollectionUtils工具类，来判断集合的返回结果是否为空，如果为空就打印一行日志
            logger.info("未找到当前分类的子分类");
        }
        return ServerResponse.createBySuccess(categoryList);
    }

    /**
     * 递归查询本节点的id及孩子节点的id
     * @param categoryId
     * @return
     */
    public ServerResponse<List<Integer>> selectCategoryAndChildrenById(Integer categoryId){
        Set<Category> categorySet = Sets.newHashSet();
        findChildCategory(categorySet,categoryId);


        List<Integer> categoryIdList = Lists.newArrayList();
        if(categoryId != null){
            for(Category categoryItem : categorySet){
                categoryIdList.add(categoryItem.getId());
            }
        }
        return ServerResponse.createBySuccess(categoryIdList);
    }


    //递归算法,算出子节点
    private Set<Category> findChildCategory(Set<Category> categorySet , Integer categoryId){
        //通过categoryId来查询出商品的id信息，并且加入到Set集合中
        Category category = categoryMapper.selectByPrimaryKey(categoryId);
        if(category != null){
            categorySet.add(category);
        }
        //查找子节点,递归算法一定要有一个退出的条件
        List<Category> categoryList = categoryMapper.selectCategoryChildrenByParentId(categoryId);
        //通过foreach循环来遍历出商品的子节点，最后返回categorySet
        for(Category categoryItem : categoryList){
            findChildCategory(categorySet,categoryItem.getId());
        }
        return categorySet;
    }






}
