package com.mmall.service.impl;

import com.google.common.collect.Lists;
import com.mmall.service.IFileService;
import com.mmall.util.FTPUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.UUID;
@Service("iFileService")
@Slf4j
public class FileServiceImpl  implements IFileService {

    //打印日记的目的是可以查看，服务经常被调用打印日记会更加有效的排查问题
//    private Logger logger = LoggerFactory.getLogger(FileServiceImpl.class);

    //要把上传的文件名返回,防止上传的图片重名
    public String upload(MultipartFile file,String path){
        String fileName =file.getOriginalFilename();

        //获取扩展名 如1.jpg，从后面查直到查到有.结束
        String fileExtensionName = fileName.substring(fileName.lastIndexOf(".")+1);
        //修改上传文件的名字，
        String uploadFileName = UUID.randomUUID().toString()+"."+fileExtensionName;
        log.info("开始上传文件，上传文件的文件名：{},上传的路径：{},新文件名：{}",fileName,path,uploadFileName);

        //创建目录
        File fileDir = new File(path);
        if(!fileDir.exists()){
            //可写权限
            fileDir.setWritable(true);
            fileDir.mkdirs();
        }
        //创建文件名
        File targetFile = new File(path,uploadFileName);

        try {
            file.transferTo(targetFile);
//            todo 将targetFile上传到FTP服务器上
            FTPUtil.uploadFile(Lists.newArrayList(targetFile));//已经上传到ftp服务器
            log.info("上传到ftp服务器");
//            todo 上传完后，删除upload下面的文件
            targetFile.delete();
        } catch (IOException e) {
            log.error("上传文件异常", e);
        }

        return  targetFile.getName();
    }


    //测试用
//    public static void main(String[] args) {
//        String file = "1.jpg";
//        System.out.println(file.substring(file.lastIndexOf(".")+1));
//    }
}
